// Пример 1
var x = 6;
var y = 14;
var z = 4;
var res;

x += y - x++ * z;
res = x;
document.write ("пример 1: x += y - x++ * z " + "результат = " + res + "<br />");

var x = 6;
var y = 14;
var z = 4;
var res1;

res1 = x + y;
document.write ("1. Выполняем операцию сложения двух переменных x (6) и y (14) = 20" + "<br />"); 

var res2;
res2 = x++ * z;
document.write ("2. постфиксный инкремент х++ (6) умножаем на z (4) = 24" + "<br />");

var res;
res = res1 - res2;
document.write ("3. От первого результата отнимаем второй результат 20-24 = -4" + "<br /> <hr>");

// Пример 2
var x = 6;
var y = 14;
var z = 4;
var res3;

z = --x - y * 5;
res3 = z;
document.write ("пример 2: z = --x - y * 5 " + "результат = " + res3 + "<br />");

var x = 6;
var y = 14;
var z = 4;
var res4;

res4 = y * 5;
document.write ("1. В приоритете операция умножения y (14) на 5 = 70" + "<br />"); 

var res3;
res3 = --x - res4;
document.write ("2. от префиксного декремента --х (5) отнимаем результат  res4 = -65" + "<br /> <hr>");

// пример 3
var x = 6;
var y = 14;
var z = 4;
var res5;

y /= x + 5 % z;
res5 = y;
document.write ("пример 3: y /= x + 5 % z " + "результат = " + res5 + "<br />");

var x = 6;
var y = 14;
var z = 4;

res6 = 5 % z;
document.write ("1. в приоритете у нас деление по модулю 5 % 4 = 1" + "<br />"); 

res7 = x + res6;
document.write ("2. к x (6) добавляем результат res6 (1) = 7" + "<br />"); 

res8 = y / res7;
document.write ("3. выполняем операцию деления y (14) делим на res7 (7) = 2" + "<br /> <hr>"); 

// пример 4
var x = 6;
var y = 14;
var z = 4;
var res9;

z = z - x++ + y * 5;
res9 = z;
document.write ("пример 4: z = z - x++ + y * 5 " + "результат = " + res9 + "<br />");

var x = 6;
var y = 14;
var z = 4;

res10 = y * 5;
document.write ("1. В приоритете умножение: y (14) * 5 = 70" + "<br />"); 

res11 = z - x++;
document.write ("2. От z (4) вычитаем постфиксный инкремент х++ (6) = -2" + "<br />"); 

res9 = res11 + res10;
document.write ("3. Выполняем операцию сложения результатов res11 (-2) и res10 (70) = 68" + "<br /> <hr>"); 

// пример 5
var x = 6;
var y = 14;
var z = 4;
var res12;

x = y - x++ * z;
res12 = x;
document.write ("пример 5: x = y - x++ * z " + "результат = " + res12 + "<br />");

var x = 6;
var y = 14;
var z = 4;

res13 = x++ * z;
document.write ("1. В приоритете умножение: постфиксный инкремент х++ (6) на z (4) = 24" + "<br />"); 

res12 = y - res13;
document.write ("2. Вычитаем от y (14) результат res13 (24) = -10" + "<br />"); 